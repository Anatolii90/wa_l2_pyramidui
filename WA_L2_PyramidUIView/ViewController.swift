//
//  ViewController.swift
//  WA_L2_PyramidUIView
//
//  Created by Anatolii on 3/23/19.
//  Copyright © 2019 Anatolii. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //рисуем квадрат с заданными параметрами
        let xPos = 400
        let yPos = 300
        let wight = 50
        let height = 50
        let step = 10
        let numberOfBoxes = 3
        // drawBox()
        //нарисуем три квадрата в линию
        drawLineBox(xPos: xPos,
                    yPos: yPos,
                    wight: wight,
                    height: height,
                    step: step,
                    numberOfBoxes: numberOfBoxes)
        //нарисуем лесенку
        //        drawLedderBox(xPos: xPos,
        //                      yPos: yPos,
        //                      wight: wight,
        //                      height: height,
        //                      step: step,
        //                      numberOfBoxes: numberOfBoxes,
        //                      pyramid: false)
        //нарисуем пирамидку
        drawLedderBox(xPos: xPos,
                      yPos: yPos,
                      wight: wight,
                      height: height,
                      step: step,
                      numberOfBoxes: numberOfBoxes,
                      pyramid: true)
    }
    
    func drawBox(xPos: Int,
                 yPos: Int,
                 wight: Int,
                 height: Int) {
        let box = UIView.init(frame: CGRect(x: xPos, y: yPos, width: wight, height: height))
        box.backgroundColor = UIColor.blue
        view.addSubview(box)
    }
    
    func drawLineBox(xPos: Int,
                     yPos: Int,
                     wight: Int,
                     height: Int,
                     step: Int,
                     numberOfBoxes: Int) {
        var currXPos = xPos
        for _ in 0..<numberOfBoxes {
            drawBox(xPos: currXPos, yPos: yPos, wight: wight, height: height)
            currXPos = currXPos+wight+step
            
        }
    }
    
    func drawLedderBox(xPos: Int,
                       yPos: Int,
                       wight: Int,
                       height: Int,
                       step: Int,
                       numberOfBoxes: Int,
                       pyramid: Bool) {
        var currXPos = xPos
        var currYPos = yPos
        var counter = numberOfBoxes
        for _ in 0..<numberOfBoxes {
            drawLineBox(xPos: currXPos,
                        yPos: currYPos,
                        wight: wight,
                        height: height,
                        step: step,
                        numberOfBoxes: counter)
            if pyramid {
                currXPos = currXPos+wight/2
            }
            else {
                currXPos = xPos
            }
            currYPos = currYPos-height-step
            counter-=1
        }
    }
}

